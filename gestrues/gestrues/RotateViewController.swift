//
//  RotateViewController.swift
//  gestrues
//
//  Created by alejo on 17/1/18.
//  Copyright © 2018 alejo. All rights reserved.
//

import UIKit

class RotateViewController: UIViewController {

    @IBOutlet weak var customView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        var x = 0;
        // Do any additional setup after loading the view.
    }

    @IBAction func rotate(_ sender: Any) {
        customView.backgroundColor = .green
    }
    @IBAction func rotate2(_ sender: Any) {
        customView.backgroundColor = .blue
    }
    @IBAction func press(_ sender: Any) {
        customView.backgroundColor = .green
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
