//
//  SwipeViewController.swift
//  gestrues
//
//  Created by alejo on 17/1/18.
//  Copyright © 2018 alejo. All rights reserved.
//

import UIKit

class SwipeViewController: UIViewController {
    @IBOutlet var swipeRight: UISwipeGestureRecognizer!
    @IBOutlet weak var customView: UIView!
    
    @IBAction func swioeUp(_ sender: Any) {
        let action = sender as! UISwipeGestureRecognizer
        customView.backgroundColor = .green
    }
    @IBAction func swipRight(_ sender: Any) {
        customView.backgroundColor = .red
    }
    
    @IBAction func swipeDown(_ sender: Any) {
        customView.backgroundColor = .blue
    }
    
    @IBAction func swipeLeft(_ sender: Any) {
        customView.backgroundColor = .purple
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
