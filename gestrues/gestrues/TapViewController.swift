//
//  TapViewController.swift
//  gestrues
//
//  Created by alejo on 17/1/18.
//  Copyright © 2018 alejo. All rights reserved.
//

import UIKit

class TapViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    @IBOutlet weak var tochesLabel: UILabel!
    @IBOutlet weak var tapsLabel: UILabel!
    @IBOutlet weak var customView: UIView!
    @IBOutlet weak var cordLabel: UILabel!
    let taps = 0
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        let touchCount = touches.count
        let tap = touches.first
        let tapCount = tap?.tapCount
        
        tochesLabel.text = "\(touchCount ?? 0)"
        tapsLabel.text = "\(tapCount ?? 0)"
        
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        let touch = touches.first
        let point = touch?.location(in: self.view)
        let x = point?.x
        let y = point?.y
        
        cordLabel.text = ("x: \(x!), y: \(y!) ")
        
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        print("termino")
    }
    
    @IBAction func tagGestrueAction(_ sender: UITapGestureRecognizer) {
    
        customView.backgroundColor = .green
    }
    

}
